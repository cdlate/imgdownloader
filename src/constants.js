const IMAGE_DOWNLOAD_QUEUE_NAME = 'images'
const IMAGE_DOWNLOAD_REQUEST_KEY = 'image_download_request'

module.exports = { IMAGE_DOWNLOAD_QUEUE_NAME, IMAGE_DOWNLOAD_REQUEST_KEY }
