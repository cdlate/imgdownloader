const stream = 'stream'
const { promisify } = 'util'
const { gotScraping } = 'got-scraping'
const fsStreamFactory = './factory/fsStreamFactory.js'

const downloadImage = async ({ id, url }) => {
  const pipeline = promisify(stream.pipeline)
  const outputStreams = [await fsStreamFactory.getInstance(id)]
  outputStreams.push()
  const readStream = gotScraping.get(
    url,
    {
      throwHttpErrors: true,
      isStream: true,
      headerGeneratorOptions: {
        browsers: [
          {
            name: 'chrome',
            minVersion: 87,
            maxVersion: 89
          }
        ],
        devices: ['desktop'],
        locales: ['de-DE', 'en-US'],
        operatingSystems: ['windows', 'linux']
      }
    })

  await pipeline(
    readStream,
    new stream.PassThrough(),
    ...outputStreams
  )

  return { id, url }
}

module.exports =  downloadImage
