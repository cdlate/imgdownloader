const { IMAGE_DOWNLOAD_QUEUE_NAME, IMAGE_DOWNLOAD_REQUEST_KEY } = require('./constants.js')

test('constants', () => {
  expect(IMAGE_DOWNLOAD_QUEUE_NAME).toBe('images')
  expect(IMAGE_DOWNLOAD_REQUEST_KEY).toBe('image_download_request')
})
