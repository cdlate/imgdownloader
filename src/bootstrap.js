const queueFactory = require('./factory/queueFactory.js')
const workerFactory = require('./factory/workerFactory.js')
const loggerSingleton = require('./singleton/loggerSingleton.js')
const downloadImage = require('./downloadImage.js')
const dotenv = require('dotenv')
const { IMAGE_DOWNLOAD_QUEUE_NAME, IMAGE_DOWNLOAD_REQUEST_KEY } = require('./constants.js')

dotenv.config()
const bootstrap = async () => {
  const logger = await loggerSingleton.getInstance({
    name: process.env.APP_NAME,
    safe: true,
    level: process.env.LOG_LEVEL
  })

  const connection = {
    connection: {
      host: process.env.REDIS_IP,
      port: process.env.REDIS_PORT
    }
  }
  await queueFactory.getInstance(
    IMAGE_DOWNLOAD_QUEUE_NAME,
    {
      limiter: {
        max: 1,
        duration: 1000,
        groupKey: 'domainId'
      },
      defaultJobOptions: {
        attempts: 3,
        backoff: {
          type: 'exponential',
          delay: 1000
        }
      },
      connection
    }
    ,
    logger
  )
  await workerFactory.getInstance(
    IMAGE_DOWNLOAD_QUEUE_NAME,
    IMAGE_DOWNLOAD_REQUEST_KEY,
    downloadImage,
    {
      limiter: {
        max: 1,
        duration: 1000,
        groupKey: 'domainId'
      },
      connection
    },
    logger
  )
  logger.debug('Bootstrap done!')
}

module.exports = bootstrap
