const pino = require('pino')

class loggerFactory {
  static async getInstance (config = {}) {
    return pino(config)
  }
}

module.exports =  loggerFactory
