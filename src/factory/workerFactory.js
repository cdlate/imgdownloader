const { Worker, QueueScheduler } = require('bullmq')

class workerFactory {
  static async getInstance (queueName, jobName, processor, config = {}, logger) {
    logger.debug(`Worker factory: instantiating worker on queue: ${queueName}`, config)
    // eslint-disable-next-line no-unused-vars
    const scheduler = new QueueScheduler(queueName, config)
    return new Worker(
      queueName,
      async (job) => {
        if (job.name === jobName) {
          await processor(job.data)
        }
      },
      config)
  }
}

module.exports =  workerFactory
