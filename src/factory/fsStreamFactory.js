const fs = require('fs')

class fsStreamFactory {
  async validate (id) {
    if (!id || !parseInt(id) || id <= 0) {
      throw new Error('Fs stream factory: invalid argument config.id')
    }
  }

  static async getInstance (id, logger) {
    logger.debug('Fs Write Stream Factory', id)
    this.validate(id)
    const path = './data/test' + id + '.jpg'
    const stream = fs.createWriteStream(path)
      .on(
        'error',
        () => {
          stream.destroy()
          fs.unlink(path, (e) => {})
        })
    return stream
  }
}

module.exports =  fsStreamFactory
