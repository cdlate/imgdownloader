const { Queue, QueueEvents, Job } = require('bullmq')

class queueFactory {
  static async getInstance (queueName, config = {}, logger) {
    logger.debug(`Queue factory: instantiating queue: ${queueName}`, config)
    const queue = new Queue(queueName, config)
    const queueEvents = new QueueEvents(queue.name, config)

    queueEvents.on('completed', async ({ jobId }) => {
      const job = await Job.fromId(queue, jobId)
      logger.debug(`Job completed ${jobId}`, job)
    })

    queueEvents.on('failed', (stuff) => {
      logger.warning(`Job failed ${stuff.id}`)
    })

    return queue
  }
}

module.exports = queueFactory
