const loggerFactory = require('../factory/loggerFactory.js')

let instance
const loggerSingleton = {}
loggerSingleton.getInstance = async (config) => {
  if (instance === undefined) {
    instance = await loggerFactory.getInstance(config)
  }
  return instance
}

module.exports = loggerSingleton
